def test_cash_flow_formula_integrity(book):
    sheet = book.sheets[0]
    sheet['start_value'].value = 100
    sheet['inflows'].value = 10
    sheet['outflows'].value = -5
    assert sheet['end_value'].value == 160


def test_vba_mysum(book):
    vba_mysum = book.macro('Module1.MySum')
    assert vba_mysum(1, 5) == 6
